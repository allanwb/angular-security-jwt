import { JwtHttp } from './../jwt-http';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario:string;
  password:string;
  lancamentosUrl = 'http://localhost:8080/categoria'

  constructor(private auth: AuthService,
    private http: JwtHttp) { }

  ngOnInit() {
  }

  login(){
    this.auth.login(this.usuario,this.password)
    
  }

  obter(){
    this.auth.obterNovoToken();
  }

  adicionar(): Promise<any> {
    return this.http.get(this.lancamentosUrl)
      .toPromise()
      .then(response => console.log(response));
  }
}
