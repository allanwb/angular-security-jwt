import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppSettings } from '../app.settings';
import { JwtHelperService } from '@auth0/angular-jwt';
//import { NgxPermissionsService } from 'ngx-permissions';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  jwtPayload : any;
  oauthTokenUrl = `${AppSettings.FOLDER_ENDPOINT}oauth/token`;
 


  constructor(private http: HttpClient,
    private jwt: JwtHelperService
    //private permissionsService: NgxPermissionsService

    ) { this.carregarToken()}

  login(usuario:string, senha: string){
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==')
    .set('Content-Type', 'application/x-www-form-urlencoded');
    const body =`username=${usuario}&password=${senha}&grant_type=password`;
    this.http.post(this.oauthTokenUrl, body, this.getHeaders()).
    toPromise() 
    .then(
      response => {
        this.armazenarToken(response['access_token']);
        localStorage.setItem('refresh', response['refresh_token']);
        console.log(response);
       // window.location.href = '/gint/';
      }
    )
    .catch( 
      response => {
        if(response.status === 400) {
          console.log('Usuário ou senha inválida.');
        } else if(response.status === 0){
          console.log('Verifique sua conexão com a internet.');
        } else {
          console.log('Ocorreu um problema. Contate o administrador.');
          console.log(response);
        }
      }
    )
  }


  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('refresh');
    //this.router.navigate(['/login']);
  }

  getHeaders() {
    
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==')
    .set('Content-Type', 'application/x-www-form-urlencoded');
    const options = {
      headers: headers,
      withCredentials: true
     };
    //const options = new RequestOptions({ headers: headers, withCredentials: true });
    return options;
  }

  isAccessTokenInvalido() {
    const token = localStorage.getItem('token');

    return !token || this.jwt.isTokenExpired(token);
  }


  obterNovoToken(): Promise<void> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Basic YW5ndWxhcjpAbmd1bEByMA==')
    .set('Content-Type', 'application/x-www-form-urlencoded');
    const body = `grant_type=refresh_token&refresh_token=${localStorage.getItem('refresh')}`;
    return this.http.post(this.oauthTokenUrl, body, {headers, withCredentials:true}).toPromise()
           .then(response => {
              this.armazenarToken(response['access_token']);
              console.log('novo access token criado')
              return Promise.resolve(null);
           })
           .catch(response => {
              console.error('Erro ao renovar token', response);
              return Promise.resolve(null);
           });
  }

  armazenarToken(token:string) {
    this.jwtPayload = this.jwt.decodeToken(token);
    //this.permissionsService.loadPermissions(this.jwtPayload.authorities);
    localStorage.setItem('token', token);
  }

  private carregarToken(){
    const token = localStorage.getItem('token');
    if(token){
      this.armazenarToken(token);
    }
  }

}
