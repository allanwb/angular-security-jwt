import { JwtHttp } from './security/jwt-http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';


import { AppComponent } from './app.component';
import { LoginComponent } from './security/login/login.component';
//import { NgxPermissionsModule } from 'ngx-permissions';
import { TesteComponent } from './teste/teste.component';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TesteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    //NgxPermissionsModule.forRoot(), 
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter, 
        whitelistedDomains: ['localhost:8080', 'ipojuca.digital'],
        blacklistedRoutes: [/\/oauth\/token/]
      }
    })
  ],
  providers: [JwtHelperService, JwtHttp],
  bootstrap: [AppComponent]
})
export class AppModule { }
